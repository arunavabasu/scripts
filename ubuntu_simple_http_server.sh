sudo apt update -y
sudo apt upgrade -y
sudo apt install apache2 -y
sudo systemctl enable apache2.service
sudo systemctl start apache2.service
sudo ufw allow 80/tcp comment 'accept Apache'
sudo ufw allow 443/tcp comment 'accept HTTPS connections'
