#!/bin/bash
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo docker run hello-world
# Linux post-install
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo mkdir -p /var/jenkins_home/
sudo chown -R jenkins:jenkins /var/jenkins_home/
JIP=$(curl icanhazip.com)
sudo docker run --name jenkins -d --restart=unless-stopped \
    -p 80:8080 \
    -v /var/jenkins_home:/var/jenkins_home \
    --privileged \
    --env JENKINS_ADMIN_ID=admin \
    --env JENKINS_ADMIN_PASSWORD=password123 \
    --env JENKINS_SERVER_IP=${JIP} \
    arubasu/jenkins:jcac

sleep 10