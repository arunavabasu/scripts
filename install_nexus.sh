#!/bin/bash
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo docker run hello-world
# Linux post-install
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker

sudo mkdir -p /opt/nexus-data
sudo chown -R 200 /opt/nexus-data

sudo docker run --name nexus -d --restart=unless-stopped \
  -p 8081:8081 \
  -v /opt/nexus-data:/nexus-data \
  --privileged \
  sonatype/nexus3:latest

sleep 10
